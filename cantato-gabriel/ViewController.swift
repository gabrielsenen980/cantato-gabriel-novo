//
//  ViewController.swift
//  cantato-gabriel
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

class  Contato {
    var nome: String
    var numero: String
    var email: String
    var endereco: String
    
    public init(nome: String, numero: String, email:String, endereco: String){
        self.nome = nome
        self.email = email
        self.endereco = endereco
        self.numero = numero
    }
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        
        return cell
        
    }
    

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view.
        listaDeContatos.append(Contato(nome: "conto 1", numero: "31 98989-0000", email: "contato1@gmail.com.br", endereco: "Rua do Cristal, 11"))
        listaDeContatos.append(Contato(nome: "conto 2", numero: "31 98989-1111", email: "contato1@gmail.com.br", endereco: "Rua do Cristal, 11"))
        listaDeContatos.append(Contato(nome: "conto 3", numero: "31 98989-2222", email: "contato1@gmail.com.br", endereco: "Rua do Cristal, 11"))
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "abrirDetalhes"{
        let detalhesViewController = segue.destination as!  DetalhesViewController
        let index = sender as! Int
        let contato = listaDeContatos[index]
        detalhesViewController.index = index
        detalhesViewController.contato = contato
        detalhesViewController.delegate = self
        detalhesViewController.contatoDelegate = self
        } else if segue.identifier == "criarContato" {
            let ContatoViewController = segue.destination as! ContatoViewController
        ContatoViewController.delegate = self
            
        }
    }
   
    

}
extension ViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "abrirDetalhes", sender: indexPath.row)
    }
}

extension ViewController: DetalhesViewControllerDelegate {
    func excluirContato(index: Int) {
        listaDeContatos.remove(at: index)
        tableView.reloadData()
    }
}

extension ViewController: ContatoViewControllerDelegate{
    func salvarNovoContato(contato: Contato) {
        listaDeContatos.append(contato)
        tableView.reloadData()
    }
    func editarContato() {
        tableView.reloadData()
    }
}
